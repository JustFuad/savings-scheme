# Accounting Microservice
## Setup
### Clone the repository
Clone the repository with:
```
git clone https://gitlab.com/JustFuad/savings-scheme
```
Then enter the `account` folder with:
```
cd savings-scheme/account/
```
### Requirements
- Python 3.9
- PostgreSQL v13+
- Docker v23.0.1+
- Docker Compose v2.16.0+

### Dependencies
Install the python dependencies with:
```
pip install -r requirements.txt
```
### Fill in the environmental variables
- Rename the `.env_example` file to `.env` and fill the following parameters:
    - Postgres database variables:
        - `DATABASE_USERNAME`: The username of your database. The default is `postgres`.
        - `DATABASE_PASSWORD`: The password of your database user. The default is `postgres`.
        - `DATABASE_HOST`: The host of your database. The default is `localhost`.
        - `DATABASE_PORT`: The port of your database. The default is `5432`.
        - `DATABASE_NAME`: The name of your database. The default is `user`.
    - Postgres test database variables:
        - `TEST_DATABASE_USERNAME`: The username of your test database. The default is `postgres`.
        - `TEST_DATABASE_PASSWORD`: The password of your test database user. The default is `postgres`.
        - `TEST_DATABASE_HOST`: The host of your test database. The default is `localhost`.
        - `TEST_DATABASE_PORT`: The port of your test database. The default is `5432`.
        - `TEST_DATABASE_NAME`: The name of your database. The default is `user`.
    - Docker variables:
        - `APPLICATION_PORT`: Port where the host machine will run the application.
        - `DATABASE_DEVELOPMENT_PORT`: Port where the host machine will connect to the database container.
        - `DATABASE_TESTING_PORT`: Port where host machine will connect to the test database container.
    - RabbitMQ variables:
        - `RABBITMQ_URL`: The RabbitMQ url connect to in the format:
        ```
        scheme://username:password@host:port/virtual_host
        ```
        The scheme can be `amqp` for non SSL connections and `amqps` for SSL connections.
## Running the app
### Locally
- Fill in the environmental variables according
- Run the app with:
```
uvicorn app.api.main:app --reload
```
### With Docker
- Fill in the environmental variables
- Build the containers with:
```
docker compose build
```
- Run the application with:
```
docker compose up
```
## API Documentation
You can find the API docs at http://127.0.0.1:8001/docs (*Use your application port number*)
## Running tests
    | In progress